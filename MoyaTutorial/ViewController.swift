//
//  ViewController.swift
//  MoyaTutorial
//
//  Created by Jerzyk on 18/09/2017.
//  Copyright © 2017 Jerzyk. All rights reserved.
//

import UIKit
import Moya

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func req1ButtonPressed () {
        let provider = MoyaProvider<MyAPI1Provider>()
        
        MyAPI1NetworkAdapter.request(target: MyAPI1Provider.cameras,
                                     success: {(response) in
                                        print("received response: \(response)")
                                        
                                        do {
                                            let cameras: [Camera] = try response.mapJSON() as [Camera]
                                            print("parsed cameras: \(cameras)")
                                        } catch {
                                            print("error while parsing data")
                                        }

        },
                                     error: {(error) in
                                        print("error: \(error.localizedDescription)")
        },
                                     failure: {(error) in
                                        print("failed: \(error)")
        })
    }
}

