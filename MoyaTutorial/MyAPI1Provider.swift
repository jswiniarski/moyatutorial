//
//  MoyaAPI1Provider.swift
//  MoyaTutorial
//
//  Created by Jerzyk on 18/09/2017.
//  Copyright © 2017 Jerzyk. All rights reserved.
//

import Foundation
import Moya

enum MyAPI1Provider {
    
    // MARK: - Cameras
    case cameras
    case settingsFor(cameraId: String)
    // MARK: - User
    case createUser(email: String, password: String)
}

extension MyAPI1Provider: TargetType {
    var baseURL: URL {
        return URL(string: "https://testing.myserver.com/api/v1")!
    }
    
    var path: String {
        switch self {
        case .cameras:
            return "/cameras"
        case .settingsFor(let cameraId):
            return "/cameras/\(cameraId)/settings"
        case .createUser:
            return "/user"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .createUser:
            return .post
        case .cameras, .settingsFor:
            return .get
        }
    }
    
    //    /// Provides stub data for use in testing.
    var sampleData: Data {
        //    TODO:
        return Data()
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .createUser(let email, let password):
            var parameters = [String: Any]()
            parameters["email"] = email
            parameters["password"] = password
            return parameters
        default:
            return nil
        }
    }
    
    /// The type of HTTP task to be performed.
    var task: Task {
        return .requestPlain
    }
    
    /// Whether or not to perform Alamofire validation. Defaults to `false`.
    var validate: Bool {
        return false
    }
    
    // The headers to be used in the request.
    var headers: [String: String]? {
        return nil
    }
    
    
    
    
    //
    //    var parameterEncoding: ParameterEncoding {
    //        return JSONEncoding.default
    //    }
    //
    //    var sampleData: Data {
    //        return Data()
    //    }
    //
    //    var task: Task {
    //        return .request
    //    }
}
