//
//  Camera.swift
//  MoyaTutorial
//
//  Created by Jerzyk on 18/09/2017.
//  Copyright © 2017 Jerzyk. All rights reserved.
//

import Mapper

class Camera: Mappable {
    var cameraId: Int?
    var title: String?
    var isAvailable: Bool?
    
    required init(map: Mapper) throws {
        try cameraId = map.from("cameraId")
        try title = map.from("title")
        try isAvailable = map.from("isAvailable")
    }
}
